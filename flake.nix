{
  outputs = { self, nixpkgs }: let
    systems = [ "x86_64-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
  in {
    overlay = final: prev: with final; {
      wlfoo = with final;
      (haskellPackages.callCabal2nix "wlfoo" ./. {}).overrideAttrs(o: {
        nativeBuildInputs = o.nativeBuildInputs or [] ++ [ pkgconfig ];
        buildInputs = o.buildInputs or [] ++ [
          wlroots wayland-scanner.dev wayland-protocols
          libxkbcommon udev pixman
        ];
      });
    };

    devShell = forAllSystems (pkgs: with pkgs; mkShell {
      inputsFrom = [ wlfoo ];
      nativeBuildInputs = with pkgs.haskellPackages; let
        haskellDeps = drv: builtins.foldl'
          (acc: type: acc ++ drv.getCabalDeps."${type}HaskellDepends")
          [ ]
          [ "executable" "library" "test" ];
      in [
        (ghcWithPackages (_: haskellDeps wlfoo))
        cabal-install
        haskell-language-server
        brittany
      ];
    });

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs // {
        default = pkgs.wlfoo;
      }
    );
  };
}
